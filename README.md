# iserv_redo
## This Documentation is a bit outdated. I will fix the outdated things in the next time.

[Issue Tracker] (https://gitlab.com/groups/schoolserver_redo_project/-/issues)
## What is this?
This is a group of applications all working together as a simple tool to serve data from an untis formatted plan behind iserv to the end user with just one click. The end-user will only see the Plan for their class, not the while confusing HTML file.

## How it works
At the very bottom of the layer we have the scrapper. [(iservscrapping)](iserv_redo.gitlab.io/scrapper.html) It logs in to iserv with given credentials and downloads the plan. To not have too much traffic on that account it uses requests-caching to cache requests to iserv.
The Scrapper lib is then used by the REST API [(iserv_redo_api)](iserv_redo.gitlab.io/api.html). The REST API serves the data over the Internet to the client in JSON format. For this a server is required.
The client is written in Javascript, CSS and HTML [(frontend)](iserv_redo.gitlab.io/frontend.html) while the actual Android App [(android-client)](iserv_redo.gitlab.io/android.html) is just a webview Wrapper loading this frontend.
The frontend then uses HTML parameters to specify server, class and light/darkmode.
To receive the data, the frontend is using jquery, which automatically converts the json to a js Object. After that, JS iterates over the objects and appends them to HTML. To have good foramtting on the HTML, its using bootstrap cards.
