# Setup a Data delivery server yourself

## Requirements
* A Server / VPS with Docker on it
* An Internetconnection

Logge dich in deinen Server per SSH ein. Gebe dann diesen Befehl ein, er erzeugt einen Container mit der Server Software auf deinem Server.
Login to your server via SSH. Type this command (of course after you corrected the values in it like usernames and urls) and it will run the server.
```
docker run \
-e iserv_user="user.name" \
-e iserv_password="deinpasswort" \
-e iserv_plan_url_today="/iserv/plan/show/raw/01-Vertreter%20Sch%C3%BCler%20heute/subst_001.htm" \
-e iserv_plan_url_tomorrow="/iserv/plan/show/raw/02-Vertreter%20Sch%C3%BCler%20morgen/subst_002.htm" \
-e iserv_base_url="https://gym-walsrode.de" \
--restart always
-p 8080:80
registry.gitlab.com/schoolserver_redo_project/schoolserver_redo_project_api:latest
```

The Server should be running at <YourDomainOrIp>:8080.
If something does not work, please file an issue.
