# Selber einen Datenlieferserver aufsetzen

## Benötigt
* Ein Server / VPS mit Docker
* Eine Internetverbindung

Logge dich in deinen Server per SSH ein. Gebe dann diesen Befehl ein, er erzeugt einen Container mit der Server Software auf deinem Server.
```
docker run \
-e iserv_user="user.name" \
-e iserv_password="deinpasswort" \
-e iserv_plan_url_today="/iserv/plan/show/raw/01-Vertreter%20Sch%C3%BCler%20heute/subst_001.htm" \
-e iserv_plan_url_tomorrow="/iserv/plan/show/raw/02-Vertreter%20Sch%C3%BCler%20morgen/subst_002.htm" \
-e iserv_base_url="https://gym-walsrode.de" \
--restart always
-p 8080:80
registry.gitlab.com/schoolserver_redo_project/schoolserver_redo_project_api:latest
```

Der Server sollte jetzt unter <DeineIpOderDomain>:8080 laufen.

WICHTIG: Der Server muss hinter einem Reverse Proxy wie zb nginx laufen, um https zu aktivieren. Aufgrund von SIcherheitsregeln im Browser ist es von der sicheren Oberfläche
nicht möglich Daten aus unverschlüsselten Quellen zu laden. Das frontend wird außerdem in Zukunft automatisch http zu https ändern.

Beispielkonfiguration:
```
server {
    server_name school.server.de;

    location / {
      proxy_pass http://127.0.0.1:88;
    }

}
```
Wobei school.server.de die Domain ist auf der das ganze laufen soll. Für diese muss ein CNAME record bei deinem DNS erstellt sein.
Diese Konfiguration enthält noch KEIN https. Es wird empfohlen https automatisch mit certbot zu konfigurieren. (kostenloses lets encrypt zertifikat)


Wenn etwas nicht funktioniert, erstelle gerne einen Issue bei dem zugehörigem Repo.
