# Summary

* [Introduction](README.md)
* [Scrapper](scrapper.md)
* [REST API](api.md)
* [Frontend](frontend.md)
* [Andoid Client](android.md)
* [Set up your own](./setup/index.md)
    * [German](setup/de.md)
    * [English](setup/en.md)


