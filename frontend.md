# Frontend

* [Source Code](https://gitlab.com/iserv_redo/frontend)

The frontend is written in Javascript, CSS and HTML.
The frontend uses HTML parameters to specify server, class and light/darkmode.
To receive the data it is using jquery ajax requests. JavaScript iterates over the objects and appends them to HTML

## Frameworks
* Bootstrap for cards and fully responsive Design

## Dependency Management
Initially, all Dependencies were on CDNs. This got changed so that its own js files are now in `src` and the external scripts like bootstrap or Cookie.js are in the `lib` folder.

The frontend can be used as a standalone PWA, which can be deployed to static hosting like netlify. Modern Web Technics like
Service Workers and Manifests are used to serve it like a normal app. This way, all clients automatically get updated every 24 hours.

Support to the old Android client is kept for now.



