# Scrapper
* [Source Code](https://gitlab.com/iserv_redo/iservscrapping)

The Scrapper for this project is written as a python module called iservscrapping. Its available on pypi

## Get started
1. Install the module using `pip3 install iservscrapping` (PyPi version is always up-to-date, thanks to CI)
2. Paste this example code:

```python
from iservscrapping import Iserv
myiserv = Iserv("https://demo-iserv.de", "user.name", "password")

print(myiserv.get_untis_substitution_plan("/iserv/plan/show/raw/vertreter/subst_002.htm", "10a"))
print(myiserv.get_next_tests(path="/iserv"))
```

## Contributing
Contributing to this project is very welcome!
By sending a PR, you agree that your code is licensed under MIT by Niwla23. You will be listed as Contributor.

## Functions

`get_untis_substitution_plan(url, schoolclass)`

Returns the substitution plan from the given url for the given class.

`get_next_tests(path="/iserv")`

Gets the the nexts classtests for the given class and given url (idesk) as Object.

`get_next_tests_formatted(path="/iserv")`

Gets the the nexts classtests for the given class and given url (idesk) formatted with dashes between them.
