# REST API

[Source Code](https://gitlab.com/schoolserver_redo_project/schoolserver_redo_project_api)

To serve the JSON data to the client, a flask application is used. It uses the iservscrapper module to get the data from the plan. To covert the python object to json, the flask-jsonpify librarie is used.

## Config
We use enviroment for config. dotenv module is currently not supported, will be added in the future.


`iserv_user`:
---

Your iserv username

`iserv_password`:
---

Your iserv password

`iserv_base_url`:
----

The base url of your iserv. For example: 
`https://demo-iserv.de`

**Note: Dont put a / at the end**

`iserv_plan_url_tomorrow`
----
The url of the plan for the next day, e.g.: `/iserv/plan/show/raw/02-Vertreter%20Sch%C3%BCler%20morgen/subst_002.htm`
**Note the leading slash**

`iserv_plan_url_today`
----
Same as `iserv_plan_url_tomorrow` but for current day.


## Dependencies:
 * Flask
 * iservscrapping (https://gitlab.com/Niwla23/iservscrapping)
 * Flask-Cors
 * Flask-Jsonpify

